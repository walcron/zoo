import fs from 'fs'
import nextjsConfig from "../next.config.mjs"

const nextjsConfigFile = "./next.config.mjs"
const staticNextJs = {
    ...nextjsConfig,
    ...{
        reactStrictMode: true,
        images: {
          unoptimized: true,
        },
        output: "export"
    }
}
const exportStatement = `export default ${JSON.stringify(staticNextJs, null, 2)}`

fs.writeFile(nextjsConfigFile, exportStatement, function writeJSON(err) {
    if (err) return console.log(err);
    console.log(JSON.stringify(staticNextJs));
    console.log('writing to ' + nextjsConfigFile);
});